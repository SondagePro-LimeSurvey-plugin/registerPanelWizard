<?php
/**
 * Allow to register with panelwizard
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2014-2015 Denis Chenu <http://www.sondages.pro>
 * @copyright 2014-2015 Validators <http://www.validators.nl>
 * @license AGPL v3
 * @version 1.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
 /**
 * Example of link
 *
 * Come to survey : http://example.org/index.php/survey/index/sid/945536/newtest/Y/lang/nl/user/vldtrs001
 */
    class registerPanelWizard extends PluginBase
    {
        static protected $description = 'A plugin to use with panelwizard (version 1.2)';
        static protected $name = 'register with Panel Wizard';
        protected $storage = 'DbStorage';

        protected $settings = array(
            'sPanelwizardUCD' => array(
                'type' => 'string',
                'default'=>'',
                'label' => 'The UCD number from panelwizard.'
            ),
        );
        // TODO :add prefill register system (in option, for hidden attribute)
        public function __construct(PluginManager $manager, $id) {
            parent::__construct($manager, $id);
            $this->subscribe('beforeSurveyPage');
            $this->subscribe('beforeSurveySettings');
            $this->subscribe('newSurveySettings');
            $this->subscribe('newDirectRequest');
        }
        function __init(){

        }
        private function getJsonSettings($sJsonSettings,$iSurveyId){
            $aSurveyLangs = Survey::model()->findByPk($iSurveyId)->getAllLanguages();
            $jSettings=$this->get("j{$sJsonSettings}", 'Survey', $iSurveyId);
            if($jSettings)
                $aActualSettings=json_decode($jSettings,true);
            else
                $aActualSettings=array();
            $aSettings=array();
            foreach($aSurveyLangs as $sLanguage){
                if(isset($aActualSettings[$sLanguage]))
                    $aSettings[$sLanguage]=$aActualSettings[$sLanguage];
                else
                    $aSettings[$sLanguage]=$this->get("bDefault{$sJsonSettings}",null,null,$this->settings["bDefault{$sJsonSettings}"]['default']);
            }
            return json_encode($aSettings);
        }
        public function beforeSurveySettings()
        {
            $oEvent = $this->event;
            self::__init();
            $iSurveyId=$oEvent->get('survey');
            $sPanelwizardSURVEY=sanitize_paranoid_string($this->get('sPanelwizardSURVEY', 'Survey', $iSurveyId,''));

            $oEvent->set("surveysettings.{$this->id}", array(
                'name' => get_class($this),
                'settings' => array(
                    'sPanelwizardSURVEY' => array(
                        'type' => 'string',
                        'default'=>'',
                        'tab'=>'tokens', // Setting no used yet
                        'category'=>'tokens', 
                        'label' => 'The survey identifier from panel wizard (Attention : the survey need a token table and set to not anonymous)',
                        'current' => $sPanelwizardSURVEY
                    ),
                    'sPanelwizardUpdate' => array(
                        'type' => 'Checkbox',
                        'value'=>'sPanelwizardUpdate',
                        'tab'=>'tokens', // Setting no used yet
                        'category'=>'tokens', 
                        'label' => 'Update and fix the url of quota and survey.',
                        'class'=>array('sPanelwizardUpdate')
                    ),
                )
            ));
            // Some script
            $assetUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets');
            Yii::app()->clientScript->registerScriptFile($assetUrl . '/js/registerpanelwizard.js');
            //Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getConfig('publicurl')."plugins/registerPanelWizard/");
            $sPanelwizardJsonurl = $this->api->createUrl('plugins/direct', array('plugin' => 'registerPanelWizard', 'function' => 'updateurl'));
            $sPanelwizardScript= "$('[id*=\"sPanelwizardUpdate\"]').click(function(e){e.preventDefault();updateSurveyUrl('{$sPanelwizardJsonurl}',{$iSurveyId});})";
            
            Yii::app()->clientScript->registerScript('sPanelwizardScript', $sPanelwizardScript,CClientScript::POS_READY);
        }
        public function newSurveySettings()
        {
            $oEvent = $this->getEvent();
            self::__init();
            foreach ($oEvent->get('settings') as $name => $value)
            {
                $this->set($name, $value, 'Survey', $oEvent->get('survey'));
            }
        }

        public function beforeSurveyPage()
        {
            $oEvent = $this->event;
            $iSurveyId = $oEvent->get('surveyId');
            self::__init();
            $sPanelwizardSURVEY=sanitize_paranoid_string($this->get('sPanelwizardSURVEY', 'Survey', $iSurveyId,""));
            $sPanelwizardUCD=sanitize_paranoid_string($this->get('sPanelwizardUCD'));
            $sPanelwizardUser=sanitize_paranoid_string(Yii::app()->request->getParam('user'));
            if(!$sPanelwizardSURVEY || !$sPanelwizardUCD)
                return;
            $sToken= Yii::app()->request->getParam('token');
            if($iSurveyId && !$sToken && $sPanelwizardUser)// Test invalid token ?
            {
                // Get the survey model
                $oSurvey=Survey::model()->findByPk($iSurveyId);
                if($oSurvey && $oSurvey->active=="Y" && Survey::model()->hasTokens($iSurveyId) && $oSurvey->anonymized=="N" )
                {
                    $sLanguage = Yii::app()->request->getParam('lang','');
                    if (!in_array($sLanguage,$oSurvey->getAllLanguages(),true))
                    {
                        $sLanguage = $oSurvey->language;
                    }
                    $aData['thissurvey'] = $aSurveyInfo = getSurveyInfo($iSurveyId,$sLanguage);
                    $aData['templatedir'] = $sTemplatePath = getTemplatePath($aSurveyInfo['template']);

                    $jUrl = @fopen("https://www.panelwizard.com/api/logic/user/get_data.json?user={$sPanelwizardUser}&survey={$sPanelwizardSURVEY}&ucd={$sPanelwizardUCD}", "rb");
                    if($jUrl)
                        $aUserInfo=json_decode(stream_get_contents($jUrl),true);
                    else
                        $aUserInfo=array("status"=>"redirect","location"=>"error");
                    if($aUserInfo['status']=="ok"){
                        // Find the user if exist
                        $oToken=TokenDynamic::model($iSurveyId)->find('firstname=:user',array(':user'=>$sPanelwizardUser));
                        // User don't exist
                        if(!$oToken){
                            // Find the attribute to fill
                            $aExtraAttributes=array();
                            foreach ($aSurveyInfo['attributedescriptions'] as $field => $aAttribute)
                            {
                                if(isset($aUserInfo['value'][$aAttribute['description']]))
                                    $aExtraAttributes[$field]=$aUserInfo['value'][$aAttribute['description']];
                            }
                            $oToken= Token::create($iSurveyId);
                            $oToken->firstname = $sPanelwizardUser;
                            $oToken->lastname = '';
                            $oToken->email = "";
                            $oToken->emailstatus = 'panelwizard';
                            $oToken->language = $sLanguage;
                            $oToken->setAttributes($aExtraAttributes);
                            $oToken->save();
                        }
                        if(!$oToken->token){
                            $iTokenId=$oToken->tid;
                            $sToken=TokenDynamic::model($iSurveyId)->createToken($iTokenId);
                        }else{
                            $sToken=$oToken->token;
                        }
                        $sRedirectUrl=Yii::app()->createUrl("survey/index",array('sid'=>$iSurveyId,'lang'=>$oToken->language,'token'=>$sToken));
                        $controller=Yii::app()->getController();
                        $controller->redirect($sRedirectUrl);
                        Yii::app()->end();
                    }else{
                        $sRedirectUrl=Yii::app()->createUrl("surveys/publicList");
                        $controller=Yii::app()->getController();
                        $controller->redirect($sRedirectUrl);
                        Yii::app()->end();
                        return;
                    }
                }
            }
        }
        public function newDirectRequest()
        {
            $oEvent = $this->event;
            if ($oEvent->get('target') == get_called_class() && $oEvent->get('function') == 'out')// Function is not needed
                $this->actionRedirect();
            if ($oEvent->get('target') == get_called_class() && $oEvent->get('function') == 'updateurl'){
                if (!$this->api->checkAccess('administrator'))
                    throw new CHttpException(403, 'This action requires you to be logged in as super administrator.');
                $this->actionUpdateUrl();
            }
        }
        // http://limesurvey.sondages.pro/plugins/direct?plugin=registerPanelWizard&function=out&sid=184133&out=out&token=5vikmf2uwh2zq6k
        public function actionRedirect()
        {
            $iSurveyId=(int)Yii::app()->request->getQuery('sid');
            $sToken=(string)Yii::app()->request->getQuery('token');
            $sOut=(string)Yii::app()->request->getQuery('out');
            $iOut=false;
            if(ctype_digit($sOut)){
                $iOut=inval($sOut);
            }else{
                switch ($sOut) {
                    case 'complete':
                        $iOut=1;
                        break;
                    case 'out':
                        $iOut=2;
                        break;
                    case 'quota':
                        $iOut=3;
                        break;
                }
            }
            $oSurvey=Survey::model()->find("sid=:sid",array(':sid'=>$iSurveyId));
            if($iOut && $oSurvey && $oSurvey->active=="Y" && Survey::model()->hasTokens($iSurveyId) && $oSurvey->anonymized=="N" )
            {
                $sPanelwizardSURVEY=sanitize_paranoid_string($this->get('sPanelwizardSURVEY', 'Survey', $iSurveyId,""));
                $sPanelwizardUCD=sanitize_paranoid_string($this->get('sPanelwizardUCD'));
                $oToken=TokenDynamic::model($iSurveyId)->find('token=:token',array(':token'=>$sToken));
                if($oToken && $oToken->token){
                    $sPanelwizardUser=sanitize_paranoid_string($oToken->firstname);
                    if(is_null($oToken->completed) || $oToken->completed=='N'){
                        $sRedirectUrl=Yii::app()->createUrl("survey/index",array('sid'=>$iSurveyId,'lang'=>$oToken->language,'token'=>$oToken->token));
                        $controller=Yii::app()->getController();
                        $controller->redirect($sRedirectUrl);
                        Yii::app()->end();
                    }
                    if($iOut==1 && $oToken->completed=='Q')
                        $iOut=2;
                        //die("https://www.panelwizard.com/api/logic/user/set_survey_status.json?user={$sPanelwizardUser}&survey={$sPanelwizardSURVEY}&ucd={$sPanelwizardUCD}&survey_status=$iOut");
                        $jUrl = @fopen("https://www.panelwizard.com/api/logic/user/set_survey_status.json?user={$sPanelwizardUser}&survey={$sPanelwizardSURVEY}&ucd={$sPanelwizardUCD}&survey_status=$iOut", "rb");
                        if($jUrl)
                            $aUserInfo=json_decode(stream_get_contents($jUrl),true);
                        else
                            $aUserInfo=array("status"=>"redirect","location"=>"error");
                        if(isset($aUserInfo['status']) && $aUserInfo['status']=='ok' && isset($aUserInfo['url'])){
                            $controller=Yii::app()->getController();
                            $controller->redirect($aUserInfo['url']);
                            Yii::app()->end();
                        }else{// An error ???
                            $sRedirectUrl=Yii::app()->createUrl("surveys/publicList");
                            $controller=Yii::app()->getController();
                            $controller->redirect($sRedirectUrl);
                            Yii::app()->end();
                        }
                }
            }else{
                $sRedirectUrl=Yii::app()->createUrl("surveys/publicList");
                $controller=Yii::app()->getController();
                $controller->redirect($sRedirectUrl);
                Yii::app()->end();
            }
        }
        private function actionUpdateUrl()
        {
            $iSurveyId=Yii::app()->request->getQuery('sid');
            $sPanelwizardSURVEY=sanitize_paranoid_string($this->get('sPanelwizardSURVEY', 'Survey', $iSurveyId,""));
            $oSurvey=Survey::model()->find("sid=:sid",array(':sid'=>$iSurveyId));
            if(!$oSurvey)
                $this->displayJson('error',"This survey don't exist");
            if(!$sPanelwizardSURVEY)
                $this->displayJson('error',"This survey don't have a panel wizard identifier");
            if($oSurvey->anonymized!='N')
                $this->displayJson('error',"This survey is anonymous");
            $sEndCompleteUrl= urldecode($this->api->createUrl('plugins/direct', array('plugin' => 'registerPanelWizard', 'function' => 'out','sid'=>$iSurveyId,'out'=>'complete','token'=>"{TOKEN}")));
            $sEndOptoutUrl= urldecode($this->api->createUrl('plugins/direct', array('plugin' => 'registerPanelWizard', 'function' => 'out','sid'=>$iSurveyId,'out'=>'out','token'=>"{TOKEN}")));
            $sEndQuotaUrl= urldecode($this->api->createUrl('plugins/direct', array('plugin' => 'registerPanelWizard', 'function' => 'out','sid'=>$iSurveyId,'out'=>'quota','token'=>"{TOKEN}")));
            $oSurvey->autoredirect="Y";
            $oSurvey->save();
            $oSurveyLanguages=SurveyLanguageSetting::model()->findAll("surveyls_survey_id=:sid",array(':sid'=>$iSurveyId));
            foreach($oSurveyLanguages as $oSurveyLanguage){
                $oSurveyLanguage->surveyls_url=$sEndCompleteUrl;
                $oSurveyLanguage->save();
            }
            // The quota
            $oQuotas=Quota::model()->findAll("sid=:sid",array(':sid'=>$iSurveyId));
            $countQuotas=array(
              "out"=>0,
              "quota"=>0,
            );
            foreach($oQuotas as $oQuota){
                $iQuotaId=$oQuota->id;
                $oQuota->autoload_url=1;
                $oQuota->save();
                if($oQuota->qlimit=="0"){
                    $countQuotas['out']++;
                    $sEndUrl=$sEndOptoutUrl;
                }else{
                    $countQuotas['quota']++;
                    $sEndUrl=$sEndQuotaUrl;
                }

                $oQuotaLanguages=QuotaLanguageSetting::model()->findAll("quotals_quota_id=:qid",array(':qid'=>$oQuota->id));
                foreach($oQuotaLanguages as $oQuotaLanguage){
                    $oQuotaLanguage->quotals_url=$sEndUrl;
                    $oQuotaLanguage->save();
                }
            }
            $message="<ul>"
              ."<li><strong>End url</strong> set to {$sEndCompleteUrl}</li>"
              ."<li><strong>{$countQuotas['out']} optout url</strong> set to {$sEndOptoutUrl}</li>"
              ."<li><strong>{$countQuotas['quota']} quota url</strong> set to {$sEndQuotaUrl}</li>"
              ."</ul>";
            $this->displayJson('success',$message);
        }
        private function displayJson($sStatus,$sMessage)
        {
            Yii::import('application.helpers.viewHelper');
            viewHelper::disableHtmlLogging();
            header('Content-type: application/json');
            echo json_encode(array(
                "status"=>$sStatus,
                "message"=>$sMessage,
                ));
            Yii::app()->end();
        }
    }
?>
